## Round 1
<p align="center">

<b>To verify Norton's Theorem in DC circuits </b> <a name="top"></a> <br>
</p>

<b>Discipline | </b> Electrical Engineering
:--|:--|
<b> Lab</b> | basic electrical lab
<b> Experiment</b>|To verify Norton's Theorem in DC circuits


<h4> [1. Focus Area](#LO)
<h4> [2. Learning Objectives ](#LO)
<h4> [3. Instructional Strategy](#IS)
<h4> [4. Task & Assessment Questions](#AQ)
<h4> [5. Simulator Interactions](#SI)
<hr>

<a name="LO"></a>
#### 1. Focus Area : instrumental and Practical Skill 
#### 2. Learning Objectives and Cognitive Level


Sr. No |	Learning Objective	| Cognitive Level | Action Verb
:--|:--|:--|:--
1.| State Norton's Theorem, Kirchhoff's current Law(KCL) and Kirchhhoff's Voltage Law(KVL). | Remember | State
2.| Identify the circuit components such as resistor, ammeter, voltmeter, multimeter etc. and it's use.  | Apply| Identify
3.| Calculate Norton's Equivalent Resistance (Rn), Norton's Equivalent Current(In) and load Current(Il).|Apply | Calculate
4.| Compare the load  current in the given circuit obtained by Norton's Theorem to KVL/KCL. | Analyse | Compare




<br/>
<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="IS"></a>
#### 3. Instructional Strategy
###### Name of Instructional Strategy  :     <u> Expository </u>
###### Assessment Method: <u>Formative and summative Assessment</u>

<u> <b>Description: <br>

•	The student will then be asked a set of pre assessment questions <br>
•       The student will perform the experiment with the help of simulator <br>
•       At last the student will be asked a set of post assessment questions <br>

<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="AQ"></a>
#### 4. Task & Assessment Questions:

Read the theory and comprehend the concepts related to the experiment.
<br>




Sr. No |	Learning Objective	| Task to be performed by <br> the student  in the simulator | Assessment Questions as per LO & Task
:--|:--|:--|:-------------------------
1.|State Norton's Theorem, Kirchhoff's current Law(KCL) and Kirchhhoff's Voltage Law(KVL). | Retrieve/Recall the statements of KCL/KVL and Norton's Theorem. | <p >The Norton current is the_____?</p> <p>a)Short circuit current </p><p> b)Open circuit current </p>       <p>c)Open circuit and short circuit</p>      <p>     d)Neither open circuit nor short circuit current</p>
2.| Identify the circuit components such as resistor, ammeter, voltmeter, multimeter etc. and it's use. | Retrieve/Recall the various circuit components such as resistor, ammeter, voltmeter. |  Norton's Theorem is true for _____  <br> a)Liner networks   <br>b)Non-Liner networks   <br>c)Both liner networks and nonliner networks  <br>d)Neither liner networks nor nonliner networks
3.| Calculate Norton's Equivalent Resistance (Rn), Norton's Equivalent Current(In) and load Current(Il). |Calculation of Norton's Equivalent Resistance (Rn) Norton's Equivalent current (In)and load current (Il). | The expresion of Norton's current (In)in the circuit shown below is? <br> a)V/Z1 <br>  b)V/Z2 <br>       c)V(Z2/(Z1+Z2)) <br>    d)VZ1(Z1+Z2)
4.| Compare the load  current in the given circuit obtained by Norton's Theorem to KVL/KCL.|Calculation of load current (Il)in the given circuit using KCL/KVL | Find R. <br> a)17.5 ohm <br>  b)17.2 ohm <br>  c)17.4 ohm <br>    d)17.8 ohm



<div align="right">
    <b><a href="#top">↥ back to top</a></b>
</div>
<br/>
<hr>

<a name="SI"></a>

#### 4. Simulator Interactions:
<br>

Sr. No |	What Students will do?| What Simulator will do?| Purpose of the task
:--|:--|:--------------------------------|-------------
1.|  Student will click on start button | Simulator will show the start simulator button | To start the simulator
2.|Student will fill in blanks for right answer with respect to statement | Simulator will show the statements with empty boxes to fill in the blanks. If the ans is correct,simulator will take to next page, if not ,a message saying retry will appear. If the answer is again wrong, the simulator will prompt the hint saying read the theory and again take the simulator.| To remember Norton's Theorem , KCL and KVL.
3.| Student will drag and drop the images against the circuit components given .|Simulator will shoe the name of circuit component along with random images of circuit components.|To identify various electrical circuit components.
4.| Student will input the values for DC supply(V<sub>1</sub>), resistors(R<sub>1</sub>,R<sub>2</sub>) and load resistor R<sub>1</sub>.|Simulator will show complex circuit with numerical input boxes.|Initiate the interaction with the experimental set-up.
5.| Student will calculate the value of R<sub>N</sub> and I<sub>N</sub> and input them in the boxes given along with R<sub>1</sub>. | Simulator will show calculate button if any of the values are wrong ,hints will appear as a prompt in the form of formula for R<sub>N</sub> and I<sub>N</sub>. If R<sub>1</sub> value is wrong ,the user will be given Rl value as a hint.Wrong values are highlighted in red and right values will be highlighted in green(box).|To get the required values of R<sub>N</sub>, I<sub>N</sub> and R<sub>L</sub>. 
6.| Student will construct Norton's equivalent circuit and click submit button. |Simulator will show the Norton's equivalent circuit with numerical input box. | To construct Norton's equivalent circuit 
7.|Student will click on calculate Il button. | simulator will give the value of load current(I<sub>L</sub>).| To calculate load current(I<sub>L</sub>).







